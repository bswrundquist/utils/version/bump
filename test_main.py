import pytest

from main import bump


BUMP_PASS_SCENARIOS = (
    ["v0.1.6", "v1.0.0-ALPHA.0", "lhs"],
    ["v0.1.6", "v0.2.0-ALPHA.0", "center"],
    ["v0.1.6", "v0.1.7-ALPHA.0", "rhs"],
    ["v0.1.6-ALPHA.3", "v0.1.6-ALPHA.4", "increment"],
    ["v0.1.6-ALPHA.3", "v0.1.6-BETA.0", "promote"],
    ["v0.1.6-CANDIDATE.3", "v0.1.6", "promote"],
    ["v0.1.6-CANDIDATE.3", "v0.1.6", "release"],
    ["v0.1.6-ALPHA.3", "v0.1.6", "release"],
)


@pytest.mark.parametrize(
    "current_version, expected_bumped_version, bump_type", BUMP_PASS_SCENARIOS
)
def test_bump_pass(current_version, expected_bumped_version, bump_type):
    actual_bumped_version = bump(version=current_version, bump_type=bump_type)

    assert expected_bumped_version == actual_bumped_version, (
        f"Actual bumped version {actual_bumped_version} "
        f"does not match the expected bumped version {expected_bumped_version}"
    )


BUMP_FAIL_SCENARIOS = (
    ["v0.1.6", "v1.0.0", "lhs"],
    ["v0.1.6", "v0.2.0", "center"],
    ["v0.1.6", "v0.1.7", "rhs"],
    ["v0.1.6-BETA.3", "v0.1.6-ALPHA.4", "increment"],
    ["v0.1.6-ALPHA.3", "v0.1.6-CANDIDATE.0", "promote"],
    ["v0.1.6-CANDIDATE.3", "v0.1.6-CANDIDATE.3", "promote"],
    ["v0.1.6-ALPHA.3", "v0.1.6-BETA.0", "release"],
)


@pytest.mark.parametrize(
    "current_version, expected_bumped_version, bump_type", BUMP_FAIL_SCENARIOS
)
def test_bump_fail(current_version, expected_bumped_version, bump_type):
    actual_bumped_version = bump(version=current_version, bump_type=bump_type)

    assert expected_bumped_version != actual_bumped_version, (
        f"Actual bumped version {actual_bumped_version} "
        f"should not match the expected bumped version {expected_bumped_version}"
    )


BUMP_ERROR_SCENARIOS = (
    ["v0.1.6-ALPHA.1", "lhs"],
    ["v0.1.6-BETA.2", "center"],
    ["v0.1.6-CANDIDATE.3", "rhs"],
    ["v0.1.6", "increment"],
    ["v0.1.6", "promote"],
    ["v0.1.6", "release"],
    ["v0.2.0", "abcdefghijk"],
    ["0.1.0", "lhs"],
    ["v1.0", "center"],
    ["v1.0.0.0", "rhs"],
    ["v0.1.0-NOTREAL.0", "release"],
)


@pytest.mark.parametrize("current_version, bump_type", BUMP_ERROR_SCENARIOS)
def test_bump_error(current_version, bump_type):
    with pytest.raises(Exception) as except_info:
        bump(version=current_version, bump_type=bump_type)

    print(except_info)
