import logging
import sys

from typing import Optional
from typing import Union

from pydantic import root_validator

from enum import Enum

from pydantic import BaseModel

import typer

app = typer.Typer()


def logger(name):
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(funcName)s - %(message)s",
    )
    return logging.getLogger(name)


class StageEnum(str, Enum):
    ALPHA = "ALPHA"
    BETA = "BETA"
    CANDIDATE = "CANDIDATE"


class VersionBumpEnum(str, Enum):
    LHS = "LHS"
    CENTER = "CENTER"
    RHS = "RHS"


class StageBumpEnum(str, Enum):
    INCREMENT = "INCREMENT"
    PROMOTE = "PROMOTE"
    RELEASE = "RELEASE"


class VersionHandler(BaseModel):
    lhs: int = 0
    center: int = 1
    rhs: int = 0

    @classmethod
    def from_string(cls, string):
        if "v" != string[0]:
            raise Exception("String version needs to start with lowercase v")
        try:
            lhs, center, rhs = [int(x) for x in string.replace("v", "").split(".")]
        except Exception:
            raise ValueError(
                f"Input version string {string} is not compatible, i.e. v0.1.0, v0.2.4"
            )
        return cls(lhs=lhs, center=center, rhs=rhs)

    def stringify(self) -> str:
        return f"v{str(self.lhs)}.{str(self.center)}.{str(self.rhs)}"

    def bump(self, bump_type: VersionBumpEnum):
        if bump_type == VersionBumpEnum.LHS:
            self.lhs += 1
            self.center = 0
            self.rhs = 0
        elif bump_type == VersionBumpEnum.CENTER:
            self.center += 1
            self.rhs = 0
        elif bump_type == VersionBumpEnum.RHS:
            self.rhs += 1


class StageHandler(BaseModel):
    stage: Optional[StageEnum] = StageEnum.ALPHA
    sequence: int = 0

    @classmethod
    def from_string(cls, string):

        string_split = string.split(".")
        if len(string_split) == 1 or string_split is None:
            stage = None
            sequence = 0
        elif len(string_split) == 2:
            stage, sequence = string_split
            stage = StageEnum[stage]
            sequence = int(sequence)
        else:
            raise Exception("Incorrect number of periods in version stage")

        return cls(stage=stage, sequence=sequence)

    def stringify(self) -> str:
        if not self.stage and not self.sequence:
            retval = ""
        else:
            retval = f"{self.stage}.{str(self.sequence)}"
        return retval

    def bump(self, bump_type: StageBumpEnum):

        if bump_type == StageBumpEnum.INCREMENT:
            self.sequence += 1

        elif bump_type == StageBumpEnum.PROMOTE:

            if self.stage == StageEnum.ALPHA:
                self.stage = StageEnum.BETA

            elif self.stage == StageEnum.BETA:
                self.stage = StageEnum.CANDIDATE

            elif self.stage == StageEnum.CANDIDATE:
                self.stage = None

            self.sequence = 0

        elif bump_type == StageBumpEnum.RELEASE:
            self.stage = None
            self.sequence = 0


class BumpHandler(BaseModel):
    version: VersionHandler
    stage: StageHandler

    bump_type: Union[VersionBumpEnum, StageBumpEnum]

    @root_validator
    def check_version_bumptype(cls, values):

        stage = values.get("stage")
        bump_type = values.get("bump_type")

        if stage.stage is None and bump_type in StageBumpEnum.__members__:
            raise Exception(  # BumpStageWihVersionOnly(
                f"Bump type {bump_type} stage present in version string"
            )
        elif stage.stage is None and bump_type in VersionBumpEnum.__members__:
            pass
        elif (
            stage.stage.name in StageEnum.__members__
            and bump_type in VersionBumpEnum.__members__
        ):
            raise Exception(  # BumpVersionWithStagePresent(
                f"Bump type {bump_type} is not compatible with stage in version string"
            )
        return values

    @classmethod
    def from_string(cls, string: str, bump_type: Union[VersionBumpEnum, StageBumpEnum]):
        string_split = string.split("-")

        if len(string_split) == 1:
            version = string_split[0]
            stage = ""
        elif len(string_split) == 2:
            version, stage = string_split

        version = VersionHandler.from_string(version)
        stage = StageHandler.from_string(stage)

        return cls(version=version, stage=stage, bump_type=bump_type)

    def stringify(self) -> str:

        if self.stage.stringify() == "":
            retval = self.version.stringify()
        else:
            retval = f"{self.version.stringify()}-{self.stage.stringify()}"

        return retval

    def bump_version(self):
        self.version.bump(bump_type=self.bump_type)
        self.stage = StageHandler()  # Default

    def bump_stage(self):
        self.stage.bump(bump_type=self.bump_type)

    def bump(self):

        if isinstance(self.bump_type, VersionBumpEnum):
            self.bump_version()

        elif isinstance(self.bump_type, StageBumpEnum):
            self.bump_stage()


bump_type_option = typer.Option("")
version_option = typer.Option("")


@app.command()
def bump(bump_type: str = bump_type_option, version: str = version_option) -> str:

    bump_type = bump_type.upper()

    bump_type_input: Union[VersionBumpEnum, StageBumpEnum]

    if bump_type in VersionBumpEnum.__members__:
        bump_type_input = VersionBumpEnum[bump_type]

    elif bump_type in StageBumpEnum.__members__:
        bump_type_input = StageBumpEnum[bump_type]

    bump_handler = BumpHandler.from_string(string=version, bump_type=bump_type_input)
    bump_handler.bump()

    output = bump_handler.stringify()

    print(output)

    return output


if __name__ == "__main__":
    app()
