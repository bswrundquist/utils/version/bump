FROM python:3.8-slim

RUN pip install typer

RUN pip install pytest
RUN pip install coverage

RUN pip install pydantic

RUN pip install pytest-subtests

ENV PYTHONUNBUFFERED True

RUN mkdir /opt/work
WORKDIR /opt/work
COPY . .

RUN pip install -e .

ENTRYPOINT [ "" ]