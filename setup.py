from setuptools import find_packages, setup

setup(
    name="bump",
    packages=find_packages(),
    version="v0.1.0-ALPHA.0",
    description="Well defined methodology for version bumping.",
    author="bswr",
    entry_points={"console_scripts": ["bump=main:app"]},
)
