FQN := bswrundquist-utils-version-bump
MOUNT_PATH := /opt/bump

build:
	docker build -t ${FQN} .

help: build
	docker run -it --rm ${FQN} --help

refresh:
	docker stop ${FQN} || true
	docker rm ${FQN} || true
	docker rmi ${FQN} || true

format:
	echo ${MOUNT_PATH}
	docker run -it --rm \
	    -v ${CURDIR}:${MOUNT_PATH} \
	    -w ${MOUNT_PATH} \
	    --entrypoint "" \
	    	registry.gitlab.com/bswrundquist/code-quality/format/black \
	    		black *.py

lint:
	docker run -it --rm \
		-v ${CURDIR}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		--entrypoint "" \
			registry.gitlab.com/bswrundquist/code-quality/lint/flake8 \
				flake8 *.py

type-check:
	docker run -it --rm \
		-v ${CURDIR}:${MOUNT_PATH}\
		-w ${MOUNT_PATH} \
			registry.gitlab.com/bswrundquist/code-quality/type-check/mypy \
				mypy *.py

unit-tests: build
	docker run -it --rm \
		--entrypoint "" \
		-v ${CURDIR}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
			${FQN} \
				bash -c "coverage run --source=main -m pytest;coverage report -m"

code-quality: format lint type-check unit-tests

fake-data: build
	docker run -it \
		--rm \
		-v ${CURDIR}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN} \
			${FQN} \
				python fake.py

smoke-test: build
	docker run -it \
		--rm \
		--env-file=.env \
		-v ${CURDIR}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN} \
			${FQN} \
				bump \
					--version v0.1.6-CANDIDATE.3 \
					--bump-type release

smoke-test-introspect: build
	docker run -it \
		--rm \
		--env-file .env \
		-v ${CURDIR}:/${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN} \
			${FQN} \
				bash

introspect:
	docker run -it --rm ${FQN} bash

site-docs:
	docker run -it \
		--rm \
		--env-file .env \
		-v ${CURDIR}:${MOUNT_PATH} \
		-w ${MOUNT_PATH} \
		-v /tmp:/tmp \
		--name ${FQN}-docs \
			docker run registry.gitlab.com/bswrundquist/docs/static-site/mkdocs \
				mkdocs serve -a 0.0.0.0:8000

clean:
	rm -rf .coverage
	rm -rf .idea
	rm -rf  __pycache__
	rm -rf *.egg-info
	rm -rf .pytest_cache
	rm -rf .mypy_cache