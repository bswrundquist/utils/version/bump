**Table of Contents**

- [bump](#bump)
- [Defining CLI](#defining-cli)
- [Commands](#commands)
  - [Unit Tests](#unit-tests)
  - [Fake data](#fake-data)
  - [Smoke Test](#smoke-test)
  - [Code Styling](#code-styling)
    - [Formating](#formating)
    - [Linting](#linting)

[![pipeline status](https://gitlab.com/bswrundquist/utils/versioning/bump/badges/master/pipeline.svg)](https://gitlab.com/bswrundquist/utils/versioning/bump/-/commits/master)
[![coverage report](https://gitlab.com/bswrundquist/utils/versioning/bump/badges/master/coverage.svg)](https://gitlab.com/bswrundquist/utils/versioning/bump/-/commits/master)

# Bump

The **Bump** CLI is used to ...

Two parts:

1. Version
    - Consists of 3 parts, **lhs**, **center**, **rhs**
    - Starts with lowercase `v`
    - Examples: v0.1.0, v2.4.3
1. Stage
    - Consists of 2 parts, **stage** and **sequence**
    - The stage is an ordered sequence (also in alphabetical order)
        - ALPHA
        - BETA
        - RELEASE
    - The sequence is an integer that represents how far along the work is in for the stage in
    - Examples: ALPHA.0, BETA.4, CANDIDATE.1

Example sequence below getting to version `v1.0.0`.

- v0.1.0-ALPHA.0 (default)
- v0.1.0-ALPHA.1 (increment)
- v0.1.0-BETA.0 (promote)
- v0.1.0-CANDIDATE.0 (promote)
- v0.1.0 (release OR promote for CANDIDATE stage)
- v0.1.1-ALPHA.0 (rhs)
- v0.1.1 (release)
- v1.0.0-ALPHA.0 (lhs)
- v1.0.0-BETA.0 (promote)
- v1.0.0 (release)

# Commands

In the *Makefile* there are `make` commands defined to simplify local development.

## Unit Tests

Unit tests allow for quick code to make sure what you are writing is clear in how it is supposed to operate but can 
also calculate the testing `coverage` of the code for one measure of code quality.

Shortcut to run unit tests locally.

```bash
make unit-tests
```
## Fake data

For quick testing there is a `fake.py` script that creates a fake *Parquet* using *Pandas* testing utilities.

To create a `test.parquet` file local to work with run below.

```bash
make fake-data
```

## Smoke Test

This will create an image and run the CLI `bump`. 

Smoke tests goes beyond unit testing by making sure it can run within an already standing infrastructure.

A generic smoke test can be run as follows. 

Look in the Makefile to add commands to the smoke test as you change the CLI. 

```bash
make smoke-test
```

## Code Styling

### Formating

Using the `black` package, the Python files are formatted using PEP8 as a guide.

The `make` shortcut can be used.

```bash
make format
```

### Linting

Linting can be done locally and is also part of the `.gitlab-ci.yml` file and it forces us to pass the tests before other
jobs can be run.

This forces code quality that helps the code be readable.

Shortcut available.

```bash
make lint
```

### Type Check

```bash
make type-check
```


